import sys
import os

print "some string to be picked up"

def CamelCaseFunction():
    """
    thus function is to try and get cognitive complexity >15 to trigger some alerts
    """
    print "This string is going to be too long and should be picked up by Sonarqube when the analysis runs, thus triggering a response to the go API call"
    if sys.argv[0]:
        for i in range(0,10):
            if i/2 < 2:
                for j in range(0,100):
                    print(i)
            else:
                for f in range(0,10):
                    print f
    else:
        pass

if __name__ == "__main__":
    print CamelCaseFunction()
