package main

import (
    "fmt"
    "encoding/json"
    "flag"
    "github.com/BurntSushi/toml"
    "gopkg.in/gomail.v2"
    "io/ioutil"
    "net/http"
    "os"
    "strings"
)

const (
    qguri = "/api/qualitygates/project_status"
    qgopts = "?projectKey="
    issueuri = "/api/issues/search?"
)

// Read the config.toml file
type sonarproj struct {
    Server server
    Mail mail
}

type server struct {
    Tag string
    Url string
    Statuses []string
    User string
    Passwd string
}

type mail struct {
    Host string
    To string
    From string
    Subject string
    User string
    Passwd string
    Port int
}

// Get information on issues
type iss struct {
    Total int
    Issues interface{}
}

type ticket struct {
    Status string
    CreationDate string
    Severity string
    Author string
    Type string
    Message string
    Component string
    Line int
}

// Get quality gate information
type qualitygate struct {
    ProjectStatus projstat
}

type projstat struct {
    Status string
}

func main() {
    conf := flag.String("conf", "", "path to the TOML config file")
    flag.Parse()

    config := readtoml(*conf)

    issues := readissues(config.Server)
    issuenum := issues.Total
    tickets := issues.Issues.([]interface{})

    outline := "There are %d issues %s\n"

    quality := getqualitygate(config.Server)
    message := fmt.Sprintf("Quality Gate - %s\n", quality.ProjectStatus.Status)
    if issuenum != 0 {
        message = message + fmt.Sprintf(outline, issuenum, "please fix")
        message = message + "The issues are detailed below\n"
        for i := 0 ; i < issuenum; i++ {
            details := issuedetails(tickets, i)
            message = message + fmt.Sprintf(
                                "Status: %s\n" +
                                "Severity: %s\n" +
                                "Type: %s\n" +
                                "Created: %s\n" +
                                "By: %s\n" +
                                "File: %s\n" +
                                "Line: %d\n" +
                                "Message:\n" +
                                "\t%s\n\n",
                                details.Status,
                                details.Severity,
                                details.Type,
                                details.CreationDate,
                                details.Author,
                                details.Component,
                                details.Line,
                                details.Message)
        }
        fmt.Println(message)
        os.Exit(1)
    } else {
        message = message + fmt.Sprintf(outline, issuenum, "")
        fmt.Println(message)
        os.Exit(0)
    }
    if config.Mail.To != "" {
        sendreport(message, config.Mail)
    }
}

func getapi(fullurl string, conf server) []byte {
    client := &http.Client{}
    request, err := http.NewRequest("GET", fullurl, nil)
    if conf.User != "" {
        request.SetBasicAuth(conf.User, conf.Passwd)
    }
    response, err := client.Do(request)

    if err != nil {
        fmt.Println("Error fetching data %s", err)
        os.Exit(1)
    }
    defer response.Body.Close()
    data, _ := ioutil.ReadAll(response.Body)

    return data
}

func readissues(conf server) iss {
    stats := strings.Join(conf.Statuses, ",")
    fullurl := conf.Url + issueuri + "componentRoots=" + conf.Tag + "&statuses=" + stats
    data := getapi(fullurl, conf)
    var m iss
    if err := json.Unmarshal(data, &m); err != nil{
        fmt.Println("Error in JSON %s", err)
        os.Exit(1)
    }
    return m
}

func getqualitygate(conf server) qualitygate {
    fullurl := conf.Url + qguri + qgopts + conf.Tag
    data := getapi(fullurl, conf)
    var qg qualitygate
    if err := json.Unmarshal(data, &qg); err != nil {
        fmt.Println("Error gettign quality gate info ", err)
        os.Exit(1)
    }
    return qg
}

func readtoml(conf string) sonarproj {
    var config sonarproj
    if _, err := toml.DecodeFile(conf, &config); err != nil {
        fmt.Println(err)
    }
    return config
}

func issuedetails(tickets []interface{}, iter int) ticket {
    var t ticket
    tjson, _ := json.Marshal(tickets[iter]);
    if err := json.Unmarshal(tjson, &t); err != nil {
        fmt.Printf("Error in Tickets JSON: %s\n", err)
    }
    return t
}

func sendreport(report string, mailconf mail) {
    fmt.Println("mail")
    m := gomail.NewMessage()
    m.SetAddressHeader("From", mailconf.From, "")
    m.SetAddressHeader("To", mailconf.To, "")
    m.SetHeader("Subject", mailconf.Subject)
    m.SetBody("text/plain", report)

    d := gomail.NewDialer(mailconf.Host,
                          mailconf.Port,
                          mailconf.User,
                          mailconf.Passwd)

    if err := d.DialAndSend(m); err != nil {
        fmt.Println(err)
    }
    return
}
