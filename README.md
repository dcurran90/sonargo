# sonargo
Go code for connecting to Sonarqube and pulling useful information

# How to use
download the sonargo file from /bin directory

```bash
user@host:~/path/to/sonargo/bin$ ./sonargo -conf=../examples/example.toml
```

This will connect to the API at the host defined in the .toml and return the Quality gate along with details of any issues

# How it works
## configuration
Configuration is held in .toml files. A simple config will require a URL, tag (project-key) and statuses (What issue statuses you want to see). You can also configure report by email, this will require a host, to address, from address, subject and username/password if using SMTP auth.

```toml
title = "Sonarcloud toml"

[server]
tag = "schizoid90-goexample"
url = "https://sonarcloud.io"
statuses = ["OPEN", "REOPENED", "CONFIRMED"]

[mail]
to = "user@example.com"
from = "mail@example.com"
user = "user"
passwd = "pass"
subject = "sonarqube report"
host = "mydomain.com"
```

Statuses are passed as a list and interpreted by sonargo.

Remove the [mail] section to not send email reports.

## code
The code will take this URL, tag and list of statuses to create the proper 'full url' in order to get the quality gate (/api/qualitygates/project_status?) and issues (/api/issues/search?). 

project keys are used to specify which project you want the data for (projectKey=**your-project**) the statuses are appended for issues to filter only the statuses you want to see (componentRoots=**your-project**&statuses=**OPEN,REOPENED,CONFIRMED**)

# Output
Output will look like the below:

## no issues
```
Quality Gate - OK
There are 0 issues 
```

## issues
```
Quality Gate - ERROR
There are 8 issues please fix
The issues are detailed below
Status: OPEN
Severity: CRITICAL
Type: CODE_SMELL
Created: 2018-03-19T17:12:08+0000
By: juniordev@devcom.com
File: your-project:file.py
Line: 394
Message:
	Refactor this function to reduce its Cognitive Complexity from 23 to the 15 allowed.
```
